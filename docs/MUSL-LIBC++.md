# Build the musl libC with C++ for morello using morello-sdk (Experimental)

This document covers the steps to build the [musl libC](https://git.morello-project.org/morello/musl-libc) from sources using morello-sdk.

**Note :** The support is **experimental**.

## Usage

### Build Musl

Create the following workspace structure:

```
musl-libc++/
  |-> workspace/
       |-> musl.env
  |-> docker-compose.yml
```

Create a `docker-compose.yml` file and map the morello directory into `musl/` as follows:

```
# Docker composer file for Morello musl
version: '3.8'
services:
  musl-morello-sdk:
    image: "git.morello-project.org:5050/morello/morello-sdk/morello-sdk:latest"
    container_name: "musl-morello-sdk"
    volumes:
      - ./workspace:/home/morello/workspace
    tty: true
    restart: unless-stopped
```

Clone the `musl` you want to build in `musl/workspace`:
```
cd musl/workspace
git clone https://git.morello-project.org/morello/musl-libc.git musl
```

Then, bring up the container (from `musl/)`:
```
$ docker-compose up -d
```

**Note:** It is important to update always to the latest version of the morello-sdk. If you are unsure on which version you are running instead of the command above execute:
```
$ docker-compose pull
$ docker-compose up -d
```

Create a `musl.env` file and map the morello directory into `musl/workspace` as follows:

```
# Create intermediates
mkdir -p $(pwd)/../.intermediates/libunwind
mkdir -p $(pwd)/../.intermediates/libcxxabi
mkdir -p $(pwd)/../.intermediates/libcxx

# Export env
export MUSL_BIN=$(realpath -s $(pwd)/../musl-bin)
export LLVM_SRC=$(realpath -s $(pwd)/../llvm)
export MORELLO=${MORELLO_HOME}/llvm
export UNWIND_BUILD=$(realpath -s $(pwd)/../.intermediates/libunwind)
export CXXABI_BUILD=$(realpath -s $(pwd)/../.intermediates/libcxxabi)
export CXX_BUILD=$(realpath -s $(pwd)/../.intermediates/libcxx)
export SYSROOT=$MUSL_BIN
export TRIPLE=aarch64-unknown-linux-musl_purecap
```

To enter into the container, run the command:

```
$ docker exec -it -u root musl-morello-sdk /bin/bash
```

Inside the container, run the commands:
```
cd musl
source /morello/env/morello-sdk
source ../musl.env
CC=clang ./configure \
		--enable-morello \
		--target=aarch64-linux-musl_purecap \
		--prefix=${MUSL_BIN}
make
make install
```

### Clone LLVM Project

In `workspace/`:
```
git clone https://git.morello-project.org/morello/llvm-project.git llvm
```

### Build libunwind
```
cd musl/tools
bash build-morello.sh libunwind ${LLVM_SRC} ${MORELLO} ${UNWIND_BUILD} ${SYSROOT} ${TRIPLE}
```

### Build libcxxabi
```
bash build-morello.sh libcxxabi ${LLVM_SRC} ${MORELLO} ${CXXABI_BUILD} ${SYSROOT} ${TRIPLE}
```

### Build libcxx
```
bash build-morello.sh libcxx ${LLVM_SRC} ${MORELLO} ${CXX_BUILD} ${SYSROOT} ${TRIPLE}
```

## Build an example application with C++

Extend the workspace structure as follows:

```
musl-libc++/
  |-> workspace/
        ...
       |-> musl.env
       |-> sysroot.env
       |-> helloworld/
            |-> Makefile
            |-> main.cpp
  |-> docker-compose.yml
```

Create a `sysroot.env` file as follows:

```
export MUSL_HOME=$(realpath -s $(pwd)/../musl-bin)
```

Enter `helloworld/` and edit the **Makefile** inserting the code below:
```
# SPDX-License-Identifier: BSD-3-Clause
# This Makefile will compile an helloworld application using Morello Clang and the Musl libc compiled for purecap.
CXX=clang++
# ELF_PATCH is used to check that the correct elf flag for Morello is present
ELF_PATCH=morello_elf
MUSL_HOME?=../../musl-bin
CLANG_RESOURCE_DIR=$(shell clang -print-resource-dir)
OUT=./bin
# we want the same result no matter where we're cross compiling (x86_64, aarch64)
TARGET?=aarch64-linux-musl_purecap
CPPFLAGS=-lunwind -lc++abi
all:
    mkdir -p $(OUT)
    $(CXX) -c -g -march=morello+c64 \
        --target=$(TARGET) --sysroot $(MUSL_HOME) \
        main.cpp -o $(OUT)/morello-helloworld.cpp.o
    
    # Use -static to build a static binary and remove "-Wl,--dynamic-linker=/morello/musl/lib/libc.so"
    $(CXX) -fuse-ld=lld -march=morello+c64 \
        --target=$(TARGET) -Wl,-rpath,$(MUSL_HOME)/lib \
        --sysroot $(MUSL_HOME) \
        $(CPPFLAGS) \
        -rtlib=compiler-rt \
        $(OUT)/morello-helloworld.cpp.o \
        -o $(OUT)/morello-helloworld-c++ \
        -Wl,--dynamic-linker=/morello/musl/lib/libc.so 
    $(ELF_PATCH) $(OUT)/morello-helloworld-c++
    rm $(OUT)/morello-helloworld.cpp.o
clean:
    rm $(OUT)/morello-helloworld-c++
```
Edit **main.cpp** and insert the code below:
```
/* SPDX-License-Identifier: BSD-3-Clause */

#include <iostream>

int main(int argc, char **argv)
{ 
    std::cout << "Hello from Morello!!" << std::endl;

	return 0;
}
```
At this point you have everything you need. Source the development kit environment file:
```
source /morello/env/morello-sdk
source ../sysroot.env
``` 
and run:
```
make
```
If everything went well your **helloworld** binary for morello should be waiting for you in **workspace/helloworld/**.

Have a lot of fun!

**Note (1):** `make` can be substituted with `make -j<N>` where **N** is the number of cores.  
**Note (2):** Once you started the docker container the files of your project are accessible at `/home/morello/workspace/musl`.

For further information please refer to the [Docker](https://docs.docker.com/) documentation.
